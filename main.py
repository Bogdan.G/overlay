#!/usr/bin/env python

from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.factory import Factory
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.popup import Popup

import PyPDF2
import os

i = 0


class LoadDialog(FloatLayout):
    load = ObjectProperty(None)
    cancel = ObjectProperty(None)


class SaveDialog(FloatLayout):
    save = ObjectProperty(None)
    pdf3 = StringProperty('')
    text_input = ObjectProperty(None)
    cancel = ObjectProperty(None)


class Root(FloatLayout):
    loadfile = ObjectProperty(None)
    savefile = ObjectProperty(None)
    pdf1 = StringProperty('')
    pdf2 = StringProperty('')
    text_input = ObjectProperty(None)

    def dismiss_popup(self):
        self._popup.dismiss()

    def show_load(self):
        content = LoadDialog(load=self.load, cancel=self.dismiss_popup)
        self._popup = Popup(title="Load file", content=content,
                            size_hint=(0.9, 0.9))
        self._popup.open()

    def show_save(self):
        content = SaveDialog(save=self.save, cancel=self.dismiss_popup)
        self._popup = Popup(title="Save file", content=content,
                            size_hint=(0.9, 0.9))
        self._popup.open()

    def load(self, path, filename):
        global pdf1, pdf2, i
        if i == 0:
            pdf1 = os.path.join(path, str(filename))
            print(pdf1)
            self.ids.file1.text = os.path.basename(pdf1[:-2])
        if i == 1:
            pdf2 = os.path.join(path, str(filename))
            print(pdf2)
            print(os.path.basename(pdf2))
            self.ids.file2.text = os.path.basename(pdf2[:-2])
        self.dismiss_popup()
        i += 1

    def save(self, path, filename):
        global pdf1, pdf2
        print(os.path.basename(pdf1[:-2]))
        print(os.path.basename(pdf2[:-2]))
        print(filename[-4:])
        minutesFile = open(os.path.basename(pdf1[:-2]), 'rb')
        pdfWatermarkReader = PyPDF2.PdfFileReader(
                           open(os.path.basename(pdf2[:-2]), 'rb'))
        pdfReader = PyPDF2.PdfFileReader(minutesFile)
        minutesFirstPage = pdfReader.getPage(0)
        minutesFirstPage.mergePage(pdfWatermarkReader.getPage(0))
        pdfWriter = PyPDF2.PdfFileWriter()
        pdfWriter.addPage(minutesFirstPage)
        for pageNum in range(1, pdfReader.numPages):
            pageObj = pdfReader.getPage(pageNum)
            pdfWriter.addPage(pageObj)
        if filename[-4:] != '.pdf':
            filename += '.pdf'
        resultPdfFile = open(os.path.join(path, filename), 'wb')
        pdfWriter.write(resultPdfFile) minutesFile.close() resultPdfFile.close() self.dismiss_popup() 

class Overlay(App):
    pass


Factory.register('Root', cls=Root)
Factory.register('LoadDialog', cls=LoadDialog)
Factory.register('SaveDialog', cls=SaveDialog)


if __name__ == '__main__':
    Overlay().run()
