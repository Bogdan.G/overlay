#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import PyPDF2
import os.path
from PyQt5.QtWidgets import (QApplication, QWidget,
                             QGridLayout, QPushButton,
                             QDesktopWidget, QListWidget,
                             QFileDialog)


class Overlay(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()

        self.files_to_be_overlayed = []
        self.file_to_overlay = ''

    def initUI(self):

        button_save = QPushButton('Save', self)
        button_add_1 = QPushButton('Add', self)
        button_add_2 = QPushButton('Add', self)

        button_save.clicked.connect(self.overlay)
        button_add_1.clicked.connect(self.showDialog1)
        button_add_2.clicked.connect(self.showDialog2)

        self.file_base = QListWidget()
        self.file_top = QListWidget()

        grid = QGridLayout()
        self.setLayout(grid)

        grid.addWidget(button_save, 13, 3)
        grid.addWidget(button_add_1, 12, 1)
        grid.addWidget(button_add_2, 12, 3)
        grid.addWidget(self.file_base, 1, 0, 10, 2)
        grid.addWidget(self.file_top, 1, 2, 10, 2)

        self.resize(600, 500)
        self.center()
        self.setWindowTitle('Overlay')
        self.show()

    def center(self):

        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def showDialog1(self):

        fname = QFileDialog.getOpenFileName(self, 'Open file')

        if fname[0]:
            self.file_base.addItem(os.path.basename(fname[0]))
            self.files_to_be_overlayed.append(os.path.basename(fname[0]))

    def showDialog2(self):

        fname = QFileDialog.getOpenFileName(self, 'Open file')

        if fname[0]:
            self.file_top.addItem(os.path.basename(fname[0]))
            self.file_to_overlay = os.path.basename(fname[0])

    def overlay(self):

        for file in self.files_to_be_overlayed:
            file_base = open(file, 'rb')
            file_top = PyPDF2.PdfFileReader(
                open(self.file_to_overlay, 'rb'))
            pdf_reader = PyPDF2.PdfFileReader(file_base)
            file_base_first_page = pdf_reader.getPage(0)
            file_base_first_page.mergePage(file_top.getPage(0))
            pdf_writer = PyPDF2.PdfFileWriter()
            pdf_writer.addPage(file_base_first_page)
            for page_num in range(1, pdf_reader.numPages):
                    page_obj = pdf_reader.getPage(page_num)
                    pdf_writer.addPage(page_obj)
            result_pdf_file = open(file + "overlay", 'wb')
            pdf_writer.write(result_pdf_file)
            file_base.close()
            result_pdf_file.close()


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ov = Overlay()
    sys.exit(app.exec_())
